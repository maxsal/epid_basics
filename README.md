# Epidemiology Basics

A repository for the `bookdown` resource at [maxsal.gitlab.io/epid_basics](https://maxsal.gitlab.io/epid_basics/).

It represents a compilation of notes and resources I've used to review the fundamentals of epidemiology.
